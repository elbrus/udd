#!/usr/bin/ruby

# Used by DDPO and the PTS

$:.unshift('../../rlibs')
require 'udd-db'

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)

COMQM = "select bugs_packages.source, count(*) as cnt from bugs, bugs_packages where bugs.id = bugs_packages.id and bugs.id not in (select id from bugs_merged_with where id > merged_with) and status != 'done'"
COMQ = "select bugs_packages.source, count(*) as cnt from bugs, bugs_packages where bugs.id = bugs_packages.id and status != 'done'"
NOPEND = "and bugs.id not in (select id from bugs_tags where tag in ('pending','fixed'))"
ENDQ = "group by bugs_packages.source"

srcs = {}
rc = Hash::new { 0 }
rc_m = Hash::new { 0 }
DB["#{COMQM} #{NOPEND} and severity >= 'serious' #{ENDQ}"].all.sym2str.
  each { |r| rc[r['source']] = r['cnt'] ; srcs[r['source']] = true }
DB["#{COMQ} #{NOPEND} and severity >= 'serious' #{ENDQ}"].all.sym2str.
  each { |r| rc_m[r['source']] = r['cnt'] ; srcs[r['source']] = true }

ino = Hash::new { 0 }
ino_m = Hash::new { 0 }
DB["#{COMQM} #{NOPEND} and severity in ('important','normal') #{ENDQ}"].all.sym2str.
  each { |r| ino[r['source']] = r['cnt'] ; srcs[r['source']] = true }
DB["#{COMQ} #{NOPEND} and severity in ('important','normal') #{ENDQ}"].all.sym2str.
  each { |r| ino_m[r['source']] = r['cnt'] ; srcs[r['source']] = true }

mw = Hash::new { 0 }
mw_m = Hash::new { 0 }
DB["#{COMQM} #{NOPEND} and severity in ('minor','wishlist') #{ENDQ}"].all.sym2str.
  each { |r| mw[r['source']] = r['cnt'] ; srcs[r['source']] = true }
DB["#{COMQ} #{NOPEND} and severity in ('minor','wishlist') #{ENDQ}"].all.sym2str.
  each { |r| mw_m[r['source']] = r['cnt'] ; srcs[r['source']] = true }

fp = Hash::new { 0 }
fp_m = Hash::new { 0 }
DB["#{COMQM} and bugs.id in (select id from bugs_tags where tag in ('pending','fixed')) #{ENDQ}"].all.sym2str.
  each { |r| fp[r['source']] = r['cnt'] ; srcs[r['source']] = true }
DB["#{COMQ} and bugs.id in (select id from bugs_tags where tag in ('pending','fixed')) #{ENDQ}"].all.sym2str.
  each { |r| fp_m[r['source']] = r['cnt'] ; srcs[r['source']] = true }

patch = Hash::new { 0 }
patch_m = Hash::new { 0 }
DB["#{COMQM} #{NOPEND} and bugs.id in (select id from bugs_tags where tag = 'patch') #{ENDQ}"].all.sym2str.
  each { |r| patch[r['source']] = r['cnt'] ; srcs[r['source']] = true }
DB["#{COMQ} #{NOPEND} and bugs.id in (select id from bugs_tags where tag = 'patch') #{ENDQ}"].all.sym2str.
  each { |r| patch_m[r['source']] = r['cnt'] ; srcs[r['source']] = true }

pkgs = (rc.keys + ino.keys + mw.keys + fp.keys + patch.keys).uniq.sort
pkgs.each do |pkg|
  print "#{pkg}:"
  print "#{rc[pkg]}(#{rc_m[pkg]}) "
  print "#{ino[pkg]}(#{ino_m[pkg]}) "
  print "#{mw[pkg]}(#{mw_m[pkg]}) "
  print "#{fp[pkg]}(#{fp_m[pkg]}) "
  puts "#{patch[pkg]}(#{patch_m[pkg]})"
end

DB["select distinct source from sources where distribution='debian'"].all.sym2str.hash_values.each do |r|
  next if srcs[r[0]]
  puts "#{r[0]}:0(0) 0(0) 0(0) 0(0) 0(0)"
end
